# Hentai crawler

Hentai4.me crawler.
hentai.crawler use core.async.

## Usage

		lein run

usage:

		$ lein run -- --hep
		 Switches               Default      Desc              
		 --------               -------      ----              
		 -h, --no-help, --help  false        Show help         
		 -s, --start            1            Set start page    
		 -e, --end              1            Set end page      
		 -o, --output-path      /tmp/hentai  Set download path 

## License

Copyright © 2013 Yutaka Matsubara

Distributed under the Eclipse Public License either version 1.0 
