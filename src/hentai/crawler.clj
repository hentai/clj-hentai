(ns hentai.crawler
  (:require
    [clj-http.client :as client]
    [clojure.data.codec.base64 :as b64]
    [clojure.data.json :as json]
    [clojure.java.io :as io])
  (:import
    [java.io File]
    [java.net URLDecoder]
    [java.util.regex Pattern]
    [org.jsoup.nodes Element]
    [java.util.zip ZipEntry ZipOutputStream Deflater])
  (:use
    [clojure.tools.logging]
    [jsoup.soup]))


(def ^:dynamic *base-url* "http://hentai4.me/")
(def ^:dynamic *page-url* "http://hentai4.me/page/%s")
(def ^:dynamic *metadata-url* "http://hentai4.me/ajax.php?dowork=getimg&id=%s&host=%s")
(def ^:dynamic *download-path* "/tmp/hentai")

(def ^:private img-id-re (Pattern/compile "/gallery-(\\d+)-(\\d+)" Pattern/DOTALL))
(def ^:private img-name-re (Pattern/compile "\\d+-hentai4.me-(\\d+).jpg" Pattern/DOTALL))

(def ^:dynamic *buffer-size* 8096)

;;
;; Utility
;;

(defn create-archive [^File dir]
  (let [buffer (byte-array *buffer-size*)
        zip-file (str (.getName dir) ".zip")
        output-file (str (io/file *download-path* zip-file))]
    (if-not (.exists (io/file output-file))
      (with-open [zip-output-stream (ZipOutputStream. (io/output-stream output-file))]
        (doseq [^File file (file-seq dir)]
          (if (.isFile file)
            (do
              (.putNextEntry zip-output-stream (ZipEntry. (.getName file)))
              (with-open [input (io/input-stream file)]
                (loop []
                  (let [bytes-read (.read input buffer 0 *buffer-size*)]
                    (when (not= -1 bytes-read)
                      (.write zip-output-stream buffer 0 bytes-read)
                      (recur)))))
              (.closeEntry zip-output-stream))))
        output-file)
      (debug "over write?" output-file))))

(defn delete-imgs [^File dir]
  (do
    (doseq [^File file (file-seq dir)]
      (when (.isFile file)
        (io/delete-file file)))
    (io/delete-file dir)))

(defn get-img-id [url]
  (let [[_ img-id host-id] (re-find (re-matcher img-id-re url))]
    (debug "get img url" url img-id host-id)
    [img-id host-id]))

(defn- create-img-url [metadata img-name]
  (format "http://%s.hdporn4.me/%s/%s" (:host metadata) (:folder_link metadata) img-name))

(defn- convert-seq-file [img-name]
  (let [[_ ^String s] (re-find (re-matcher img-name-re img-name))]
    ; (println img-name)
    (str (Integer. s) ".jpg") ))

(defn ^File get-parent-file [metadata]
  (let [parent (str (:folder_link metadata))]
    (when parent
      (io/file *download-path* parent))))

(defn- create-filename [metadata img-name]
  (let [folder (str (:folder_link metadata))
        f (io/file *download-path* folder (convert-seq-file img-name))
        parent (get-parent-file metadata)]
    ; (debug "folder" parent)
    (.mkdirs parent)
    f))

;;
;; parse 
;;

(defn get-hentai-links [data]
  ($ (parse data)
     "a[rel=bookmark]"
     (mapv #(.attr ^Element % "href"))))

(defn get-gallery-link [url]
  (let [data (slurp url)]
    (first
      ($ (parse data)
         "a[href^=/gallery]"
         (mapv #(.attr ^Element % "href"))))))

(defn get-img-metadata [[img-id host-id]]
  (if img-id
    (let [data (slurp (format *metadata-url* img-id, host-id))
          json-str (String. ^bytes (b64/decode (.getBytes (URLDecoder/decode data "UTF-8")))) ] 
      (json/read-str json-str :key-fn keyword))
    (debug "*" img-id)))

(defn get-last-page []
  (let [link (first
               ($ (parse (slurp *base-url*))
                  ".last"
                  (mapv #(.attr ^Element % "href"))))]
    (Integer. ^String (last (clojure.string/split link #"/")))))

;;
;; download
;;

(defn- write-file [path data]
  (with-open [w (io/output-stream path)]
    (.write w ^bytes data)))

(defn- get-img [url]
  (let [h {"User-Agent" "Mozilla/5.0 (Windows NT 6.1;) Gecko/20100101 Firefox/13.0.1"}]
    (debug "download-img" url)
    (:body (client/get url {:headers h :as :byte-array}))))

(defn download-img [metadata img-name]
 (let [url (create-img-url metadata img-name)
       path (create-filename metadata img-name)]
   (write-file path (get-img url))
   path))


