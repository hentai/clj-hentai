(ns hentai.core
  (:require
   [clojure.core.async :as async :refer :all])
  (:use
   [hentai.crawler]
   [clojure.tools.logging]
   [clojure.tools.cli :only [cli]]))

(defmacro go* [chan & body]
  `(go
    (try
      ~@body
      (catch Exception e#
        (do
          (error e# (.getMessage e#))
          (>! ~chan e#))))))

(defmacro not-error [x & body]
  `(when-not (= Exception (type ~x))
     ~@body))

(defn- zipper [a b]
  (mapv (fn [x y] (vector x y)) a b))

(defn- download-imgs* [pc metadata]
  (let [names (:img_name metadata)
        n (count names)
        cs (repeatedly n chan)
        parent (get-parent-file metadata)]
    (debug "start" parent)
    (doseq [[img-url c] (zipper names cs)]
      (go* c 
           (>! c (download-img metadata img-url))))
    (dotimes [i n]
      (let [[v c] (alts!! cs)]
        (not-error v c)))
    (go* pc
         (>! pc
             (do
               (create-archive parent)
               (delete-imgs parent)
               (debug "deleteted" parent)
               true)))))

(defn- get-hentai-metadata [link c]
  (go* c
       (>! c (->
              (get-gallery-link link)
              (get-img-id)
              (get-img-metadata)))))

(defn- download-imgs [n cs]
  (loop [i n cs cs result []]
    (if (> i 0)
      (let [[v _] (alts!! cs)]
        (if (:folder_link v)
          (let [c (chan)]
            (go * c (download-imgs* c v))
            (recur (dec i) cs (conj result c)))
          (recur (dec i) cs result)))
      result)))

(defn- crawl-gallery [links]
  (let [n (count links)
        cs (repeatedly n chan)]
    (doseq [[link c] (zipper links cs)]
      (get-hentai-metadata link c))

    (let [cs (download-imgs n cs)
          n (count cs)]
      (dotimes [i n]
        (let [[v c] (alts!! cs)]
          (not-error v c))))
    "OK"))

(defn- crawl-hentai [start end]
  (let [n (- (inc end) start)
        cs (repeatedly n chan)]
    (doseq [[idx c] (map-indexed vector cs)]
      (go* c
           (>! c (get-hentai-links (slurp (format *page-url* (+ idx start)))))))
    (dotimes [i n]
      (let [[v c] (alts!! cs)]
        (not-error v
                   (crawl-gallery v))))
    (debug "fin")))

(defn -main [& args]
  (let [[options args banner] (cli args
                                   ["-h" "--help" "Show help" :default false :flag true]
                                   ["-s" "--start" "Set start page" :default 1 :parse-fn #(Integer. %)]
                                   ["-e" "--end" "Set end page" :default 1 :parse-fn #(Integer. %)]
                                   ["-o" "--output-path" "Set download path" :default "/tmp/hentai"]
                                   )]
    (when (:help options)
      (println banner)
      (System/exit 0))
    (let [start (:start options)
          end (:end options)
          output-path (:output-path options)]
      (binding [*download-path* output-path]
        (crawl-hentai start end)))))


